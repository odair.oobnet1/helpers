const user1 = {
    name:'odair',
    id:1,
    value:'eaeaeaeaea'
 }
 
 const user2 = {
    name:'davi',
    id:2,
    value:'eaeaeaeaea'
 }
 
 const user3 = {
    name:'davi',
    id:2,
    value:'eaeaeaeaea'
 }
 
 const users = [user1, user2, user3]
 
 
 let keys = ['id']
 
 const newUSer = {}
 
 /**
 *
 * @param {*} obj
 * @param {*} keys
 * Funçao para trazer apenas o campo determinado de um objecto
 */
 
 const filterObject = (obj, keys ) => {
    return keys
            .map( element => ( { [ element ] : obj[ element ] } ) )
            .reduce( ( actualValue, currentValue ) => {
                 return {
                     ...actualValue,
                     ...currentValue
            } }, {} )
 }
 
 /**
 *
 * @param {*} obj
 * @param {*} keys
 * Funçao para remover determinados campos em um objeto
 */
 
 const extractFieldForObj = (obj, extractKeys) => {
    return Object.keys(obj)
                 .filter( key =>  !extractKeys.includes(key) )
                 .map( key => ( { [key]: obj[key]  } ) )
                 .reduce( (actualValue, currentValue) => {
                    return {
                        ...actualValue,
                        ...currentValue
                    }
                 }, {} )
 }
 
 /**
 *
 * @param {*} obj
 * @param {*} keys
 * Funçao para  trazer determinados campos em um array de objeto
 */
 const filterForMapArrayObject = keys => obj => filterObject(obj, keys)
 
 /**
 *
 * @param {*} obj
 * @param {*} keys
 * Funçao para remover determinados campos em um array de objeto
 */
 const extractFieldForMapArray = keys => obj => extractFieldForObj(obj,keys)
 
 
 const removeItensRepeatFormMapArrays = (array, el) => {
   
    let objs = {}
 
    array.map ( ell =>  {
        if(ell[el] &&  !objs[ell[el]]){
            objs[ell[el]]= ell
        }
    } )
 
    return Object.values(objs)
 }
 
 
 console.log(removeItensRepeatFormMapArrays(users,'id'))
 
 